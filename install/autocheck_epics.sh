########## IDEA

# let us write an auto install file which is capable of 
# finding the EPICS installation and list all the modules
# install dependencies missing
# check the gpib drivers and stuff
# build and check if IOC runs

# Run chmod +x ./autoinstall.sh
# ./autoinstall.sh 
# use Sudo privileges in case of error

# apt-get update -y && apt-get upgrade -y && \
#     apt-get install -y --no-install-recommends \
#     ca-certificates \
#     libntirpc-dev libtirpc3 \
#     tzdata \
#     build-essential \
#     busybox \
#     git \
#     python3-minimal \
#     python3-pip \
#     python3-venv \
#     re2c \
#     rsync \
#     ssh-client \
#     && rm -rf /var/lib/apt/lists/* \
#     && busybox --install

# sudo apt autoremove



### Check if Epics is already installed
# if ! command -v caget &> /dev/null 
# then
#     echo "EPICS is Installed"
# else
#     echo "EPICS is not installed."
#     echo "Installing"
# fi

### check if a string in the path environment variables
if [ -z "$EPICS_HOST_ARCH" ]
then
    echo "EPICS_HOST_ARCH not present"
else 
    echo "create it"
fi

result=$(echo $PATH | tr ":" "\n" | grep "epics")

if [ -n "$result" ]; then
    echo "The substring is present in the variable."
    echo "$result"
else
    echo "The substring is NOT present in the variable."
fi



### check if a folder exists, otherwise create it
# if [ -d "/opt/epics"]
# then
#     echo "OK"
# else 
#     echo "Creating folder..."
#     mkdir -p "$folder_path"
#     echo "Folder create successfully"
# fi