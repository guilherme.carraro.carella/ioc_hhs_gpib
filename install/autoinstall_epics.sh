#!/bin/bash

# Set environment variables (OK)
export EPICS_HOST_ARCH=linux-x86_64
export EPICS_BASE=/opt/epics/base
export SUPPORT=/opt/epics/support
export LC_ALL=C
export IOCS=/opt/epics/ioc
export TZ=Europe/Berlin

ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone  

echo "export EPICS_HOST_ARCH=linux-x86_64" >> ~/.bashrc
echo "SUPPORT=/opt/epics/support" >> ~/.bashrc
echo "EPICS_BASE=/opt/epics/base" >> ~/.bashrc
echo "export LC_ALL=C" >> ~/.bashrc

# Install build tools and utilities (OK)
apt-get update -y
apt-get upgrade -y
apt-get install -y --no-install-recommends \
    ca-certificates \
    libntirpc-dev libtirpc3 \
    tzdata \
    build-essential \
    busybox \
    git \
    python3-minimal \
    python3-pip \
    python3-venv \
    re2c \
    rsync \
    ssh-client \
    dkms \
    lsb-release \
    lsb-core \
    libgconf-2-4 \
    libgtk-3-0 \
    libasound2 \
    libcanberra-gtk-module \
    libcanberra-gtk3-module \
    x11-apps \
    software-properties-common

git config --global advice.detachedHead false

# Create important folders (OK)
mkdir /opt/epics
mkdir /opt/epics/base
mkdir /opt/epics/support
mkdir /opt/epics/ioc

# Clone the EPICS repository (OK)
git clone --depth 1 --recursive --branch R7.0.7 https://github.com/epics-base/epics-base.git ${EPICS_BASE}

# Build EPICS (OK)
make -C ${EPICS_BASE} -j $(nproc)

# create RELEASE.local file (OK)
touch ${SUPPORT}/RELEASE.local
echo "EPICS_BASE=${EPICS_BASE}">> ${SUPPORT}/RELEASE.local
echo "SUPPORT=${SUPPORT}">>${SUPPORT}/RELEASE.local

# install autosave (OK)
git clone --depth 1 --recursive --branch R5-10-2 https://github.com/epics-modules/autosave.git ${SUPPORT}/autosave
make -C ${SUPPORT}/autosave -j $(nproc)
echo "AUTOSAVE=${SUPPORT}/autosave">> ${SUPPORT}/RELEASE.local

# install seq (OK)
git clone --depth 1 --recursive --branch vendor_2_2_8 https://github.com/ISISComputingGroup/EPICS-seq.git ${SUPPORT}/seq
make -C ${SUPPORT}/seq -j $(nproc)
echo "SEQ=${SUPPORT}/seq">> ${SUPPORT}/RELEASE.local

# install sscan (OK)
git clone --depth 1 --recursive --branch R2-11-5 https://github.com/epics-modules/sscan.git ${SUPPORT}/sscan
sed -i 's/^SNCSEQ=/#SNCSEQ=/' ${SUPPORT}/sscan/configure/RELEASE # important
make -C ${SUPPORT}/sscan -j $(nproc)
echo "SSCAN=${SUPPORT}/sscan">> ${SUPPORT}/RELEASE.local

# install calc (OK)
git clone --depth 1 --recursive --branch R3-7-4 https://github.com/epics-modules/calc.git ${SUPPORT}/calc
make -C ${SUPPORT}/calc -j $(nproc)
echo "CALC=${SUPPORT}/calc">> ${SUPPORT}/RELEASE.local

# install asyn (OK)
git clone --depth 1 --recursive --branch R4-44-2 https://github.com/epics-modules/asyn.git ${SUPPORT}/asyn
sed -i '/TIRPC/s/^#//g' ${SUPPORT}/asyn/configure/CONFIG_SITE
make -C ${SUPPORT}/asyn -j $(nproc)
echo "ASYN=${SUPPORT}/asyn">> ${SUPPORT}/RELEASE.local

# install busy (OK)
git clone --depth 1 --recursive --branch R1-7-4 https://github.com/epics-modules/busy.git ${SUPPORT}/busy
make -C ${SUPPORT}/busy -j $(nproc)
echo "BUSY=${SUPPORT}/busy">> ${SUPPORT}/RELEASE.local

# install pcre (OK)
git clone --depth 1 --recursive --branch R8-44 https://github.com/chrschroeder/pcre.git ${SUPPORT}/pcre
make -C ${SUPPORT}/pcre -j $(nproc)
echo "PCRE=${SUPPORT}/pcre">> ${SUPPORT}/RELEASE.local

# install stream (OK) 
git clone --depth 1 --recursive --branch 2.8.24 https://github.com/paulscherrerinstitute/StreamDevice.git ${SUPPORT}/stream
# sed -i 's/^CALC=/#CALC=/' ${SUPPORT}/stream/configure/RELEASE # important
make -C ${SUPPORT}/stream -j $(nproc)
echo "STREAMDEVICE=${SUPPORT}/stream">> ${SUPPORT}/RELEASE.local

# Clone IOC repository
git clone --depth 1 --recursive --branch dev_tests https://codebase.helmholtz.cloud/guilherme.carraro.carella/ioc_hhs_gpib.git /opt/epics/ioc/ioc_hhs_gpib

# install Keysight libraries
chmod +x ${IOCS}/ioc_hhs_gpib/iolibrariessuite-installer_20.1.29718.0.run
${IOCS}/ioc_hhs_gpib/iolibrariessuite-installer_20.1.29718.0.run --mode unattended

# install keysight library kernels
chmod +x ${IOCS}/ioc_hhs_gpib/iokerneldrivers-installer_20.1.29718.0.run 
${IOCS}/ioc_hhs_gpib/iokerneldrivers-installer_20.1.29718.0.run --mode unattended

# install visa epics
git clone --depth 1 --recursive https://github.com/ISISComputingGroup/EPICS-VISA.git ${SUPPORT}/visa
sed -i 's#\$(APPNAME)_SYS_LIBS_Linux += visa#\$(APPNAME)_SYS_LIBS_Linux += iovisa#' ${SUPPORT}/visa/visa_lib.mak
sed -i 's#USR_INCLUDES += -I/usr/include/ni-visa#USR_INCLUDES += -I/opt/keysight/iolibs/include#' ${SUPPORT}/visa/VISAdrvApp/src/Makefile
sed -i '/\$(APPNAME)_DBD += VISAdrv.dbd/a \$(APPNAME)_DBD += calc.dbd' ${SUPPORT}/visa/VISAdrvTestApp/src/build.mak
sed -i '/\$(APPNAME)_LIBS += stream VISAdrv asyn/s/$/ calc/' ${SUPPORT}/visa/VISAdrvTestApp/src/build.mak
make -C ${SUPPORT}/visa clean install

# build ioc
cp ${IOCS}/ioc_hhs_gpib/configure/RELEASE.local ${IOCS}/ioc_hhs_gpib
make -C ${IOCS}/ioc_hhs_gpib clean install -j $(nproc)