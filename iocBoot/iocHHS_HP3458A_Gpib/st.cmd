#!../../bin/windows-x64-mingw/HHS_HP3458A_Gpib

#- You may have to change HHS_HP3458A_Gpib to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

epicsEnvSet("STREAM_PROTOCOL_PATH", "${TOP}/protocols")

## Register all support components
dbLoadDatabase "dbd/HHS_HP3458A_Gpib.dbd"
HHS_HP3458A_Gpib_registerRecordDeviceDriver pdbbase

## Register serial port
drvAsynVISAPortConfigure("L0", "GPIB0::22::INSTR")

## Load record instances
dbLoadRecords("db/HHS_HP3458A_Gpib.db","PORT=L0, P=DVM:")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=a3558"
