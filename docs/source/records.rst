Records
=======

This part is a copy of the db file, and it is generated by the docs/source/auto-epics-records.py . Some things are important for the automatic generation to work properly:

* Titles are found in db file by starting with "####".
* lines starting by only one "# " are added as text (note the space after #)


.. auto-epics-records:: ../db/HHS_HP3458A_Gpib.db