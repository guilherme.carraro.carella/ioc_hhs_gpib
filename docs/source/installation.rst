Installation
===============

This IOC relies on various EPICS modules and device support, contingent upon the specific device in use. For our setup, the PCI-GPIB card from Keysight is utilized locally. Consequently, it is imperative to install the Keysight IO Libraries Suite (IOLS) along with the associated kernel drivers.

.. note::

   Keysight IO Libraries Suite facilitates instrument communication across a range of Linux distributions compatible with GPIB, USB, USB-GPIB, LAN, and RS-232 test instruments from various manufacturers.

   Keysight IO Kernel Drivers installs a suite of kernel drivers for USBTMC, USB-to-GPIB (82357) converter, or PCI/PCIe GPIB cards.

Automatic Installation
----------------------

If you lack EPICS or any essential dependencies, utilize the /install/autoinstall_epics.sh script. There is no need to clone the repository; simply copy the script's content and paste it into a local bash file. Execute the following commands:

.. code-block:: console
   
   chmod +x ./autoinstall_epics.sh
   
   ./autoinstall_epics.sh

.. note::
   
   Ensure that the user has the necessary permissions to modify the "/opt" folder. Use sudo commands (for Ubuntu) or assign ownership of the folder to the current user.

Manual Installation
--------------------

For a customized installation, refer to the steps outlined in /install/autoinstall_epics.sh.

.. literalinclude:: ../../install/autoinstall_epics.sh
   :language: bash 

Docker Installation
--------------------

While attempting to containerize this IOC, challenges arose in communication with the hardware. Although the /install/autoinstall_epics.sh script can be tested within a Docker container, communication with the device remains problematic. Even deploying the container with the --privileged flag or building the devices did not resolve these issues. Consider exploring alternative methods for containerizing this IOC.
