Usage
=====

Settings
--------------------

In the settings file (/iocBoot/iocHHS_HP3458A_Gpib/st.cmd), it is crucial to configure the path where EPICS should search for the protocol, as well as specify the VISA port to which the device is connected. Additionally, the database file must be loaded. Refer to the file below.

.. literalinclude:: ../../iocBoot/iocHHS_HP3458A_Gpib/st.cmd

User Interface
----------------

The user interface, developed with `CSS Phoebus <https://controlssoftware.sns.ornl.gov/css_phoebus/>`_, aims to empower users to control and monitor the device's status. The interface is divided into four main parts:

* **Sample box (black background):** Displays sample status, showcasing measurement range, function (DCV, ACV, OHM, FREQ...), and the calculated field rate in samples per second.

* **Control box with Buttons:** Includes buttons for reset, hold, auto, sample, read memory, and save array.
   * Reset button sends a "RESET" command via GPIB.
   * Hold stops sampling using "TARM HOLD".
   * Auto sends "TARM AUTO," which resumes sampling based on TRIG and Event settings.
   * Sample, in addition to changing the machine status, reads values from the device.
   * Read memory sends a command "RMEM 1,%(n),1," reading the memory based on record "n" (record size should match MCOUNT).
   * Save array writes the array from the device to a txt file (currently located at "/opt"). Consider creating custom automations for measurements.

   The control box also allows sending and querying values through ASCII commands, detailed in the command reference on the left. An error message box is updated in case of errors. Be cautious, as this record is updated multiple times, and it's easy to miss errors. It's recommended to use camonitor DVM:ERRMSG during startup.

* **Memory Settings box (gray background):** Displays the current memory status, including memory mode, data type format, and array size. Update fields by pressing the update button.

* **Measurement Settings box (gray background):** Shows the current settings for making measurements.

The command reference on the left of the User Interface provides guidance on each command and parameter's functionality.

To query a parameter, use "<command>?", for example, "TIMER?" or "TRIG?". Note that "NRDGS?" returns an array, which is also updated in the "Event" and "Readings" in the measurement settings box.

Update buttons were added as setting a scan time overloaded communication, causing some commands not to be processed or the multimeter to stop responding. This may not be an issue with a newer device.

Note that update commands are not possible while the DVM is in sampling mode or running multiple measurements.

.. image:: ../figures/ui.png
   :alt: User Interface
   :align: center


Example
----------------

Start the IOC by

.. code-block:: console

   cd ioc_hhs_gpib/iocBoot/iocHHS_HP3458A_Gpib

   ../../bin/linux-x86_64/HHS_HP3458A_Gpib st.cmd


Open your Phoebus instance and load the dmm.bob file. 

#. Update all the panels by pressing the update button. This is necessary because initialization might not update all the fields.

#. Check if the "Run Single" button is working. The current measurement should be shown in the display (if DISP ON) and the interface.

#. To perform multiple measurements, send commands in the "SEND" box (refer to the manual for detailed information):

   .. code-block:: console

      PRESET FAST;APER 8.0E-3;MFORMAT ASCII;MEM FIFO;TIMER 4.70e-5;RANGE 10.0;NRDGS 1000,AUTO;TARM SGL,1;OFORMAT ASCII;RMEM 1,1000,1

   #. Update the fields on the screen to double-check if the memory is not empty - MCOUNT field.

   #. Sending these commands will not read the memory and put it inside our EPICS record RMEM. To do that, press the "Read Memory" button. Now the record $(P)RMEM contains the read array.

   #. Save the array in a file named "Output.txt" located in the /opt folder. Consider creating your own automations if necessary.

.. note::
   
   This ensures functionality and may be useful for quick measurements. However, for general use, it is recommended to write, for example, a Python automation using pyepics or bluesky/ophyd.

.. note:: 

   Access the manual from the button to explore additional command sequences and customizations.
