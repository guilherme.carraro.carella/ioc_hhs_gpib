

### SEQUENCE OLD HHS ###

PRESET FAST # specifies NORM, FAST or DIG
APER 8.0E-3 # Specifies the A/D converter integration time in seconds
MFORMAT DINT # Clears reading memory and designates the new storage format
MEM FIFO # memory mode, FIFO clears memory and store new readings
TIMER 4.70e-5 # time inserted between readings
RANGE 10.0 # select a measurement range or the autorange mode.
NRDGS 1000,EXTSYN # number of readings NRDGS [count],[event]
TARM SGL,1 # enables the trigger event TARM [event],[numer_arms]

### DVMReadOut ###
OFORMAT ASCII # Designates the GPIB output format
RMEM 1,1000,1 # return group of readings in the memory RMEM [first],[count],[record]

### RESET DISPLAY ###
TIMER 0.5
AZERO ON
TARM AUTO
TRIG AUTO
DISP ON
NRDGS 1000, TIMER
RESET

## to copy ##
PRESET FAST;APER 8.0E-3;MFORMAT ASCII;MEM FIFO;TIMER 4.70e-5;RANGE 10.0;NRDGS 1000,AUTO;TARM SGL,1;OFORMAT ASCII;RMEM 1,1000,1
## to copy ##
TARM HOLD;DISP OFF;ARANGE OFF;AZERO OFF;MEM FIFO;TIMER 0.5;RANGE 10;NRDGS 10,TIMER;OFORMAT ASCII;MFORMAT ASCII

docker run -it --device=/dev/pcigpib0:/dev/pcigpib0 -v /sys/devices:/sys/devices -v /sys/class:/sys/class temp

docker run --privileged -it temp