HP3458A EPICS IOC
=================

Welcome to the HP3458A EPICS IOC project! This initiative involves the development of an EPICS (Experimental Physics and Industrial Control System) IOC (Input/Output Controller) for the HP3458A Multimeter. The IOC utilizes the stream-device protocol within the EPICS Control System to establish communication via GPIB with the HP3458A.

Link Documentation:

https://ioc-hhs-gpib-guilherme-carraro-carella-30e153a455d4a48b3f6252fa.pages.hzdr.de/

Installation, Usage, and Protocol
----------------------------------

For detailed information on installation, usage instructions, and the communication protocol, please refer to the respective sections in the documentation:

- [Installation](#installation): Learn how to set up the required dependencies and install the Keysight IO Libraries Suite for seamless communication with the HP3458A.

- [Usage](#usage): Explore the settings, user interface, and an example of how to interact with the HP3458A through the EPICS IOC.

- [Protocol](#protocol): Gain insights into the communication protocol based on the EPICS Control System stream-device, covering various data types, commands, and functionalities.

Feel free to explore the provided documentation and adapt the EPICS IOC to meet your specific requirements. If you encounter any issues or have questions, reach out to the project's community for assistance.

Happy measuring with the HP3458A EPICS IOC!
