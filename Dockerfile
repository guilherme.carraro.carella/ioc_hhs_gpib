## set base image with generic keithley
FROM registry.hzdr.de/hzb/epics/ioc/images/adsimdetectorgenericimage:latest AS base

## set default shell for next commands
SHELL ["/bin/bash", "-c"]

# install packages 
RUN apt-get update &&  apt-get install dkms lsb-release lsb-core libgconf-2-4 gawk iptables libxss1 libappindicator1 libunwind8 python3-pip libx11-xcb1 libgtk-3-0 libasound2 pandoc python3-pip -y

RUN pip install pyvisa pyvisa-py gpib-ctypes psutil zeroconf matplotlib pyepics pandoc numpy nbsphinx

# copy files
# COPY iolibrariessuite-installer_20.1.29718.0.run /opt
# COPY iokerneldrivers-installer_20.1.29718.0.run /opt 
# COPY configure/RELEASE.local /opt/epics/support

# prepare environment
RUN export SUPPORT=/opt/epics/support && \
    mkdir /opt/epics/ioc && \
    export IOCS=/opt/epics/ioc && \
    git config --global advice.detachedHead false && \
    echo "export LC_ALL=C" >> ~/.bashrc

# clone ioc
RUN cd /opt/epics/ioc && \
    git clone --depth 1 --recursive https://codebase.helmholtz.cloud/guilherme.carraro.carella/ioc_hhs_gpib.git && \
    cp /opt/epics/ioc/ioc_hhs_gpib/configure/RELEASE.local ${SUPPORT} && \
    cp /opt/epics/ioc/ioc_hhs_gpib/configure/RELEASE.local /opt/epics/ioc/RELEASE.local

# # install iols
RUN sudo chmod +x /opt/epics/ioc/ioc_hhs_gpib/iolibrariessuite-installer_20.1.29718.0.run && \
    # sudo chmod +x /opt/epics/ioc/ioc_hhs_gpib/iokerneldrivers-installer_20.1.29718.0.run  && \ 
    sudo /opt/epics/ioc/ioc_hhs_gpib/iolibrariessuite-installer_20.1.29718.0.run --mode unattended
    # sudo /opt/epics/ioc/ioc_hhs_gpib/iokerneldrivers-installer_20.1.29718.0.run --mode unattended

# install pcre
RUN git clone --depth 1 --recursive --branch R8-44 https://github.com/chrschroeder/pcre.git ${SUPPORT}/pcre
RUN make -C ${SUPPORT}/pcre -j $(nproc)

# install stream
RUN git clone --depth 1 --recursive --branch 2.8.24 https://github.com/paulscherrerinstitute/StreamDevice.git ${SUPPORT}/stream
RUN make -C ${SUPPORT}/stream -j $(nproc)

# install visa epics
RUN git clone --depth 1 --recursive https://github.com/ISISComputingGroup/EPICS-VISA.git ${SUPPORT}/visa && \
    sed -i 's#\$(APPNAME)_SYS_LIBS_Linux += visa#\$(APPNAME)_SYS_LIBS_Linux += iovisa#' ${SUPPORT}/visa/visa_lib.mak && \
    sed -i 's#USR_INCLUDES += -I/usr/include/ni-visa#USR_INCLUDES += -I/opt/keysight/iolibs/include#' ${SUPPORT}/visa/VISAdrvApp/src/Makefile && \
    sed -i '/\$(APPNAME)_DBD += VISAdrv.dbd/a \$(APPNAME)_DBD += calc.dbd' ${SUPPORT}/visa/VISAdrvTestApp/src/build.mak && \
    sed -i '/\$(APPNAME)_LIBS += stream VISAdrv asyn/s/$/ calc/' ${SUPPORT}/visa/VISAdrvTestApp/src/build.mak &&\
    make -C ${SUPPORT}/visa clean install
